﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using NotifySystem;

public class Vitality : Member {

    public bool Dead = false;
    
    public delegate void _OnHeal();
    public delegate void _OnHurt();
    public delegate void _OnDie();

    public _OnHeal OnHeal;
    public _OnHurt OnHurt;
    public _OnDie OnDie;

    //public List<Action> OnDie = new List<Action>();

    CharacterDialogue cDialogue;

    public string cName
    {
        // if we have a cDialogue component, use that name. Otherwise, use the gameobject name
        get { return (cDialogue ? cDialogue.CharacterName : name); }
    }

    public int Health = 4;
    public int MaxHealth = 4;
    
    public override void OnNotify(string msg, object obj) { }

    void Start()
    {
        cDialogue = GetComponent<CharacterDialogue>(); // it's okay if this is null!
        CheckStatus();
    }

    public void Heal(int amt)
    {
        Health = Mathf.Clamp(Health+amt,0,MaxHealth);

        OnHeal();

        CheckStatus();
    }

    public void Hurt(int amt)
    {
        Health = Mathf.Clamp(Health - amt, 0, MaxHealth);
        print("Oof! Took " + amt + " damage! At " + Health + " hp.");

        if (OnHurt != null)
            OnHurt();

        CheckStatus();
    }

    public void Kill() //instantly kill this character, regardless of health
    {
        Health = 0;
        CheckStatus();
    }

    public void SetMaxHealth(int amt)
    {
        MaxHealth = Mathf.Clamp(amt, 1, 100); //probably fine
        CheckStatus();
    }

    void CheckStatus()
    {
        Notify(GetName() + "_Health", Health);
        if (Health <= 0)
        {
            print("I'm dead!");
            Notify(GetName() + "_Dead", true);

            if (OnDie != null)
                OnDie();
        }
    }

    public string GetName()
    { return (cDialogue ? cDialogue.CharacterName : name); }

}
