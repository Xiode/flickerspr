﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "Game/GameRules")]
public class GameRules : ScriptableObject {

    public float Difficulty = 1f; //approaching 0 is easier (ai react slower, do less damage, etc) approaching infinity is harder. 1 is normal
    public float Gravity = 0.2f;
    public LayerMask VisionLayerMask;
    public bool AIEnabled = true;
    public NPCState defaultAIState;
    public Stats defaultStats;
    
}
