﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(menuName = "Game/Weapon Stats")]
public class WeaponStats : ScriptableObject
{
    //TODO: Add sprites, sounds, etc
    public int BaseDamage;

    [Range(0f, 3f)]
    [Tooltip("How long the player's subsequent swings should be to trigger optimal timing, in seconds")]
    public float OptimalTiming;
    [Range(0f, 0.5f)]
    [Tooltip("How late or early the player's input can be to trigger optimal timing, in seconds")]
    public float TimingRange;
    [Range(0f, 3f)]
    [Tooltip("If the player swings too early, this many seconds are added to their attack cooldown")]
    public float PunishTiming;

}
