﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "Puzzles/Puzzle")]
public class Puzzle : ScriptableObject {
    public PuzzleGoal[] goals;
    public PuzzleElement[] elements;


}


public class PuzzleElement
{
    public Interactable interactable;
}