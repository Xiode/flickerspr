﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using NotifySystem;

public class PuzzleGoal : ScriptableObject {

    public void Init(Interactable interactable)
    {

    }

    public void OnSuccess(Interactable interactable)
    {

    }

    public void OnFail(Interactable interactable)
    {

    }
}
