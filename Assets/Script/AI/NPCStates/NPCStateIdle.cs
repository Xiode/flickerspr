﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "Game/AI/States/Idle")]
public class NPCStateIdle : NPCState {

    public override void Think(NPCController npcc)
    {
        //TODO: idle anims, etc

        foreach (GameObject g in ((ZombieController)npcc).EnemyList)
        {
            if (Vector3.Distance(npcc.transform.position, g.transform.position) < npcc.stats.VisionRange &&
                ((Zombie)npcc.AIBrain).Hunt != null) //can see an enemy, and I have a hunt state! switch to it.
            {
                //TODO: target priorities? sort by range, health, attack power?
                ((ZombieController)npcc).CurrentTarget = g.transform;
                ((ZombieController)npcc).GiveUpTimer = 1;
                npcc.AIState = ((Zombie)npcc.AIBrain).Hunt;
            }
        }
    }
}
