﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "Game/AI/States/Dumb Hunt")]
public class NPCStateHunt : NPCState {
    public override void Think(NPCController npcc)
    {
        if (((ZombieController)npcc).CurrentTarget != null) //if we have a target...
        {
            if (Vector3.Distance(npcc.transform.position, ((ZombieController)npcc).CurrentTarget.transform.position) <= npcc.stats.VisionRange && //if we're in range...
                !Physics.Linecast(npcc.transform.position, ((ZombieController)npcc).CurrentTarget.transform.position, npcc.gameRules.VisionLayerMask)) //and can see target
            {
                ((ZombieController)npcc).GiveUpTimer = npcc.stats.GiveUpHuntTime; //target's close, don't give up
            }
            else //otherwise...
            {
                ((ZombieController)npcc).GiveUpTimer -= Time.deltaTime; //tick down timer. git demoralized, son
            }

            ((ZombieController)npcc).MoveTowards(((ZombieController)npcc).CurrentTarget.transform.position);
            npcc.spriteRenderer.flipX = (((ZombieController)npcc).CurrentTarget.transform.position.x < npcc.transform.position.x);

            if (((ZombieController)npcc).GiveUpTimer <= 0)
            {
                Debug.Log("OH SHIT, HE GONE.");
                ((ZombieController)npcc).CurrentTarget = null;
                ((ZombieController)npcc).MoveTowards(npcc.transform.position);
                npcc.AIState = npcc.AIBrain.IdleState; //give up on target, return to idle
            }
        }
        if (
            ((ZombieController)npcc).CurrentTarget &&
            Vector3.Distance(npcc.transform.position, ((ZombieController)npcc).CurrentTarget.transform.position) < npcc.stats.BaseRange
            && ((ZombieController)npcc).CanAttack <= 0f)
        {
            ((ZombieController)npcc).Attack();
            //MeleeAttack(npcc); //melee attack current target
        }
    }
    
    void MeleeAttack(NPCController npcc)
    {
        Debug.Log("RAarraarARUGH");

        ((ZombieController)npcc).CanAttack = npcc.stats.AttackFrequency;

        ((ZombieController)npcc).CurrentTarget.GetComponent<Vitality>().Hurt((int)npcc.stats.BaseDamage); //will do better than this
        npcc.Anim.SetTrigger("Attack");
        /*
        TODO:
        - check for collision
        */
    }
}
