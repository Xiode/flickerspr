﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "Game/AI/Brain/Zombified Crew")]
public class Zombie : Brain {
    
    public NPCState Hunt;
    
    public override void AIStart(NPCController npcc)
    {
        BuildEnemyList(npcc);
        
        ((ZombieController)npcc).TargetPosition = npcc.transform.position;
    }

    void BuildEnemyList(NPCController npcc)
    {
        //can't refesh enemies on the fly without calling Init again. maybe a physics.boxcheck / tag compare would be fine...?
        foreach (string tag in EnemyTags)
        {
            ((ZombieController)npcc).EnemyList.AddRange(GameObject.FindGameObjectsWithTag(tag));
        }
        Debug.Log("found enemies: " + ((ZombieController)npcc).EnemyList.Count);
    }

    public override void Think(NPCController npcc)
    {
        npcc.AIState.Think(npcc);

        //todo: NavMesh based movement
        if (Vector3.Distance(npcc.transform.position, ((ZombieController)npcc).TargetPosition) > .2f) //todo: make variable
        {
            npcc.Velocity = (((ZombieController)npcc).TargetPosition - npcc.transform.position).normalized;
            npcc.Velocity *= npcc.stats.MovementSpeed;
        }
        else
        {
            npcc.Velocity = npcc.Velocity.magnitude > .1 ? Vector3.zero : npcc.Velocity * Time.deltaTime;
        }
    }
    
}
