﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class NPCState : ScriptableObject {
    public abstract void Think(NPCController npcc);
}