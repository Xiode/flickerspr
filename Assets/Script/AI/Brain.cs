﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Brain : ScriptableObject {

    [Tooltip("Objects with these tags are 'enemies'")]
    public string[] EnemyTags;
    
    public NPCState IdleState;

    public abstract void AIStart(NPCController npcc);
    public abstract void Think(NPCController npcc);
}