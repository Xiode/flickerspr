﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrewController : NPCController {

	// Use this for initialization
	new void Start () {

        GetComponent<Vitality>().OnDie += () =>
        {
            Anim.SetBool("Dead", true);
        };
        base.Start();
    }
	
	// Update is called once per frame
	new void Update () {
        base.Update();
	}
}
