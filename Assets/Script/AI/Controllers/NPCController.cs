﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using NotifySystem;
using System;

[RequireComponent(typeof(CharacterController))]
[RequireComponent(typeof(Vitality))]
public class NPCController : Member
{

    CharacterController cc;
    
    public SpriteRenderer spriteRenderer;
    [HideInInspector]
    public Vector3 Velocity;
    [HideInInspector]
    public Vitality Vitals;

    public Animator Anim;

    [HideInInspector] //hide the shame!
    public string AnimState;

    public GameRules gameRules;
    public Stats stats; //character stats.
    public Brain AIBrain; //pluggable ai state machine

    [Tooltip("The NPC's starting state. If none, is set to the above GameRules' defaultAIState.")]
    public NPCState AIState; //current state

    public bool AIEnabled = true;
    
    public List<Cooldown> Cooldowns = new List<Cooldown>();

    public override void OnNotify(string msg, object obj)
    {
        if (msg == "Pause" && (int)obj == 1)
        {
            AIEnabled = false;
        }
        if (msg == "Pause" && (int)obj == 0)
        {
            AIEnabled = true;
        }
    }

    // Use this for initialization
    protected void Start()
    {
        Velocity = Vector3.zero;

        cc = GetComponent<CharacterController>();
        Vitals = GetComponent<Vitality>();

        if (AIBrain)
            AIBrain.AIStart(this);

        if (!AIState)
            AIState = gameRules.defaultAIState;

        if (!stats)
            stats = gameRules.defaultStats;
    }

    private List<Cooldown> cooldownsCompleted = new List<Cooldown>();

    // Update is called once per frame
    protected void Update()
    {
        if (cc.isGrounded)
            Velocity.y = 0f;

        Velocity.y -= gameRules.Gravity;

        if (AIBrain && gameRules.AIEnabled && AIEnabled)
        {
            AIBrain.Think(this);

            foreach (Cooldown c in Cooldowns)
            {
                if (c.Update()) //returns true when finished. updating here seems kinda awful.
                    cooldownsCompleted.Add(c); //on complete, mark for removal. I wish cooldowns could handle this, but it's not worth fussing about at the moment
            }
            foreach (Cooldown cc in cooldownsCompleted)
            {
                Cooldowns.Remove(cc);
            }
            cooldownsCompleted.Clear(); //dump list

            cc.Move(Velocity * Time.deltaTime);
        }

    }
}