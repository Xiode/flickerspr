﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;


public class ZombieController : NPCController {

    public float CanAttack = 0f;
    public float GiveUpTimer = 0f;
    public Vector3 TargetPosition;

    public List<GameObject> EnemyList; //list of enemies in the world
    public Transform CurrentTarget;

    public override void OnNotify(string msg, object obj)
    {
        base.OnNotify(msg, obj);
    }

    new void Start()
    {
        GetComponent<Vitality>().OnHurt += () =>
        {
            CanAttack = stats.AttackFrequency;
            if (GetComponent<Vitality>().Health > 0)
                Anim.SetTrigger("Hurt");
        };

        GetComponent<Vitality>().OnDie += () =>
        {
            Anim.SetBool("Dead", true);
            AIEnabled = false;
        };

        base.Start();
    }

    new void Update()
    {
        if (CanAttack > 0)
            CanAttack -= Time.deltaTime;

        base.Update();
    }

    public void MoveTowards(Vector3 tgt)
    {
        TargetPosition = tgt;
    }

    public void Attack()
    {

    }
}
