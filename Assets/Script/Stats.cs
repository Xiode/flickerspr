﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "Game/AI/Character Stats")]
public class Stats : ScriptableObject {

    public float MovementSpeed = 1f;

    [Tooltip("How far can this character detect enemies?")]
    public float VisionRange = 5f;

    [Tooltip("Base (unarmed) attack range")]
    public float BaseRange = 1f;

    [Tooltip("Base (unarmed) attack damage")]
    public float BaseDamage = 1f;

    [Tooltip("Minimum time, in seconds, between each attack")]
    public float AttackFrequency = 1f;

    [Tooltip("How long does it take this character to give up a hunt when they've lost sight of a target?")]
    public float GiveUpHuntTime = 10f;
}
