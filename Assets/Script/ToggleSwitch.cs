﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using NotifySystem;

//This component goes on a character that you want the player to be able to talk to
public class ToggleSwitch : Interactable
{

    [SerializeField]
    GameObject ChoiceBoxPrefab;
    


    PlayerUI plui;

    ChoiceBox activeChoiceBox;

    [SerializeField]
    string ActivateQuestionText;
    [SerializeField]
    string DeactivateQuestionText;

    [SerializeField]
    [Tooltip("Should text be shown? If not, text is skipped and the switch is toggled.")]
    bool ShowText = true;

    [Tooltip("Current toggle state.")]
    public bool On = false; //is the toggle on?

    [SerializeField]
    [Tooltip("Messages to be sent when toggled on")]
    string[] OnMessages;
    [SerializeField]
    [Tooltip("Messages to be sent when toggled off")]
    string[] OffMessages;
    
    public override void OnNotify(string msg, object obj) { }

    void Start()
    {
        plui = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerUI>(); //gross.
        AddObserver(plui);
    }

    void Update()
    {
        if (activeChoiceBox)
        {
        }
    }

    public override void Exec()
    {
        //1 of 3 things should occur:
        //1. descriptive text is shown, and then the player is presented with ui yes/no buttons, OR
        //2. if 1 happened, then a selection is made
        //3. the switch is just toggled with no text.

        //1. open choice box
        if (!activeChoiceBox)
        {
            if (ShowText)
            {
                activeChoiceBox = PushDialogue(On ? DeactivateQuestionText : ActivateQuestionText);
                Notify("UI_DialogueEnter");
                Notify("UI_Use");
            } else
            {
                Toggle();
            }
        }

        if (activeChoiceBox && activeChoiceBox.Selection != -1)
        {
            switch (activeChoiceBox.Selection)
            {
                case 0:
                    Notify("UI_Cancel");
                    break;
                case 1:
                    Toggle();
                    break;
            }
            ConcludeDialogue();
        }
    }

    void Toggle()
    {
        On = !On;
        Notify("UI_Use");

        if (On)
        {
            foreach (string n in OnMessages)
            {
                Notify(n);
            }
        }
        else {
            foreach (string n in OffMessages)
            {
                Notify(n);
            }
        }
    }

    void ConcludeDialogue()
    {
        PopDialogue(activeChoiceBox);

        Notify("UI_DialogueExit");
        activeChoiceBox = null;
    }
    
    ChoiceBox PushDialogue(string Question)
    {
        GameObject choicebox = Instantiate(ChoiceBoxPrefab);
        choicebox.transform.SetParent(plui.UICanvas.transform);
        choicebox.transform.position = new Vector3(plui.UICanvas.pixelRect.width/2, choicebox.transform.position.y);
        ChoiceBox c = choicebox.GetComponent<ChoiceBox>();
        c.Init(Question);
        plui.UIEventSystem.SetSelectedGameObject(c.Button1);
        c.AddObserver(plui);
        return c;
    }

    void PopDialogue(ChoiceBox c)
    {
        if (!c)
            return; //doesn't exist

        plui.RemoveObserver(c);
        GameObject.Destroy(c.gameObject);
    }
}