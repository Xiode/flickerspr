﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Dialogue/Dialogue Line Node")]
public class DialogueLineNode : DialogueNode
{
    public override DialogueLineNode Line()
    {
        return this;
    }

    public override DialogueNode NextNode()
    {
//        Debug.Log(nextNode);

        return nextNode;
    }

    [TextArea(3,10)]
    public string Text;
    public string CharacterID;
    public DialogueNode nextNode;
}
