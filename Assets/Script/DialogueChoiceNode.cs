﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using NotifySystem;

public abstract class DialogueNode : ScriptableObject {
    public abstract DialogueLineNode Line();
    public abstract DialogueNode NextNode();
}

[CreateAssetMenu(menuName = "Dialogue/Dialogue Choice Node")]
[System.Serializable]
public class DialogueChoiceNode : DialogueNode
{
    public List<DialogueChoice> Choices;
    public DialogueNode DefaultNode;

    public override DialogueLineNode Line()
    {
        Choices.Sort(
            delegate(DialogueChoice a, DialogueChoice b) // TODO: confirm this actually works lol
            {
                if (a.Conditions.Count == b.Conditions.Count)
                    return 0;
                else if (a.Conditions.Count < b.Conditions.Count)
                    return -1;
                else
                    return 1;
            }
        
        );

        foreach (DialogueChoice choice in Choices)
        {
            if (choice.Compare()) // if all the conditions are met...
            {
                if (choice.Result == this)
                {
                    Debug.LogError("Loop! Choice " + this.name + " immediately returns to self!");
                    return null; //basic infinite loop protection.
                }

                return choice.Result == null ? null : choice.Result.Line();
            }
        }

        return DefaultNode == null ? null : DefaultNode.Line();
    }

    public override DialogueNode NextNode()
    {
        return Line();
    }
}

[System.Serializable]
public class DialogueChoice
{
    public List<DialogueCondition> Conditions;
    public DialogueNode Result;

    public bool Compare()
    {
        foreach (DialogueCondition cond in Conditions)
        {
            if (!cond.Compare())
                return false;
        }
        return true;
    }
}

[System.Serializable]
public class DialogueCondition
{
    public string Key;
    public Comparison Check;
    public string Value;
    Types DesiredType;

    bool Normalized = false; // have we generated the appropriately typed value yet?

    // pre-converted types
    int ValueInt;
    float ValueFloat;
    long ValueLong;

    public void Init()
    {
        Normalized = true; //just fuck my shit up

        if (Check == Comparison.IntEqual ||
            Check == Comparison.IntLessThan ||
            Check == Comparison.IntGreaterThan)
        {
            DesiredType = Types.Int;
        } else if (Check == Comparison.StringEqual)
        {
            DesiredType = Types.String;
        }
        
        switch (DesiredType)
        {
            case Types.Int:
                ValueInt = System.Convert.ToInt32(Value);
                break;
            case Types.Float:
                ValueFloat = System.Convert.ToSingle(Value);
                break;
            case Types.Long:
                ValueLong = System.Convert.ToInt64(Value);
                break;
        }

    }

    public bool Compare()
    {
        if (!Normalized)
            Init(); //lol constructors don't work in unity

        object fact;

        try
        {
            fact = FactDictionary.GetFact(Key);
            if (Check == Comparison.Exists && fact != null)
                return true;

            fact = ((Fact)fact).Value;
        }
        catch (Exception)
        {
            if (Check == Comparison.DoesNotExist)
            {
                return true;
            }

            return false;
        }

        switch (Check)
        {
            case Comparison.StringEqual:
                return ((string)fact == Value);
            case Comparison.IntEqual:
                return ((int)fact == ValueInt);
            case Comparison.IntLessThan:
                return ((int)fact < ValueInt);
            case Comparison.IntGreaterThan:
                return ((int)fact > ValueInt);
        }
        
        return false;
    }

    public enum Comparison
    {
        Exists,
        DoesNotExist,
        StringEqual,
        IntEqual,
        IntLessThan,
        IntGreaterThan
    }
    public enum Types
    {
        String,
        Int,
        Float,
        Long
    }
}