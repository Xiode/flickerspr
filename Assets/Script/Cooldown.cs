﻿using System;
using System.Collections.Generic;
using UnityEngine;

//Cooldown
//Object that performs an Action after some amount of time. Handled by 
public class Cooldown
{
    delegate void _onComplete();
    _onComplete onComplete;
    public float Duration;
    public float TimeRemaining;
    public bool Complete = false;
    
    public Cooldown(float duration)
    {
        Duration = duration;
        TimeRemaining = duration;
    }

    public Cooldown(float duration, Action a)
    {
        Duration = duration;
        TimeRemaining = duration;
        onComplete += () => { a(); };
    }

    public void AddAction(Action a)
    {
        onComplete += () => { a(); };
    }

    public float AddTime(float time)
    {
        TimeRemaining += time;
        return TimeRemaining;
    }

    public bool Update()
    {
        TimeRemaining -= Time.deltaTime;

        if (TimeRemaining <= 0)
        {
            onComplete();
            return true;
        }
        return false;
    }
}