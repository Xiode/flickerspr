﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace NotifySystem
{
    //template for a member that acts as both an observer and a subject. below is all the notification types
    public abstract class Member : MonoBehaviour {

        protected List<Member> observers = new List<Member>();


        public void Notify(string msg)
        {
            foreach (Member o in observers)
            {
                Notify(msg, null);
            }
        }
        /*
        public void Notify(Notification msg, object obj)
        {
            foreach (Member o in observers)
            {
                o.OnNotify(msg, obj);
                FactDictionary.AddFact(msg.ToString(), obj);
            }
        }*/

        public void Notify(string msg, object obj)
        {

            foreach (Member o in observers)
            {
                o.OnNotify(msg, obj);
            }
            FactDictionary.AddFact(msg, obj);
            
        }

        public void AddObserver(Member observer)
        {
            observers.Add(observer);
        }

        public void RemoveObserver(Member observer)
        {
            observers.Remove(observer);
        }
        
        public abstract void OnNotify(string msg, object obj);

        /*
        put me in any classes that inheret Member:

        public override void OnNotify(Notification msg) { }
        public override void OnNotify(Notification msg, object obj) { }

        */
    }
    /*
    public enum Notification
    {

        //Input
        Input_DelayUse,
        Input_FreezeMotion,
        Input_UnfreezeMotion,

        //Game
        Game_ActivateTestLight, //this is terrible. replace with dictionairy system
        Game_DeactivateTestLight,

        Game_OpenDoor,
        Game_FinishOpenDoor,

        //UI
        UI_Use,
        UI_Cancel,
        UI_DialogueTick,
        UI_DialogueEnter,
        UI_DialogueExit
    }*/
}