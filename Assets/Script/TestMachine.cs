﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NotifySystem;

public class TestMachine : Member {

    Light l;
    
    public override void OnNotify(string msg, object obj) {
        switch (msg)
        {
            case "Game_ActivateTestLight":
                l.enabled = true;
                Notify("Game_Light", 1);
                break;
            case "Game_DeactivateTestLight":
                l.enabled = false;
                Notify("Game_Light", 0);
                break;
        }
    }

    public ToggleSwitch Switch; //switch that toggles this machine
    
    // Use this for initialization
    void Start () {
        l = GetComponent<Light>();
        l.enabled = false;

        Switch.AddObserver(this );

        Notify("Game_Light", 0);
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
