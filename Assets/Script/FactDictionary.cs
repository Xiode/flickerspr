﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public static class FactDictionary {
    

    public static Dictionary<string, Fact> Facts = new Dictionary<string, Fact>();

    public static void AddFact(string key, object value)
    {
        if (Facts.ContainsKey(key))
        {
            Facts[key].Value = value;
        }
        else
        {
            Facts.Add(key, new Fact(value, DateTime.Now.Ticks));

            //Debug.Log("added: " + key);
        }
    }

    // attempts to fetch a Fact from the Facts dictionary, returns null if the
    // provided key isn't present... or any other error occurs.
    public static Fact GetFact(string key)
    {
        try
        {
            return Facts[key];
        } catch
        {
            return null;
        }
    }

    // returns true if key was valid, and the entry was removed. returns false otherwise.
    public static bool RemoveFact(string key)
    {
        try
        {
            Facts.Remove(key);
            return true;
        }
        catch
        {
            return false;
        }
    }
}

public class Fact
{
    public Fact(object v, long t)
    {
        Value = v;
        TimeCreated = t;
        LastUpdated = t;
    }

    public object Value;
    public long TimeCreated;
    public long LastUpdated;
}

/*
public abstract class Fact
{
    string Key;
    object Value;
}

public class IntFact : Fact
{
    string Key;
    int Value;

    public IntFact(string key, int value)
    {
        Key = key;
        Value = value;
    }

}

public class StringFact : Fact
{
    string Key;
    string Value;

    public StringFact(string key, string value)
    {
        Key = key;
        Value = value;
    }
}

public class FloatFact : Fact
{
    string Key;
    float Value;

    public FloatFact(string key, float value)
    {
        Key = key;
        Value = value;
    }
}
*/