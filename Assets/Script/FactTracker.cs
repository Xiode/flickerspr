﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FactTracker : MonoBehaviour {

    public Text DictionaryDebug;

    string s = "";

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate()
    {
        s = "";

        foreach (KeyValuePair<string, Fact> entry in FactDictionary.Facts)
        {
            s += entry.Key + " :    " + entry.Value.Value + " / " + entry.Value.LastUpdated + " : " + entry.Value.TimeCreated + "\n";
        }

        DictionaryDebug.text = s;
    }
}
