﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using NotifySystem;
using DG.Tweening;

//this component should be attached to a trigger object that is roughly fills the volume of a room.
//it handles the camera.
public class RoomVolume : Member {

    Camera mainCam;
    GameObject pl;

    [SerializeField]
    [Tooltip("Camera distance from player")]
    float DistanceZ;

    [SerializeField]
    [Tooltip("Distance from center of RoomVolume that camera cannot pass")]
    float LimitZ;

    [SerializeField]
    float CamSmooth = 2f;

    [SerializeField]
    MeshRenderer[] Walls;

    private Color opaque;
    private Color transparent;
    private Color semitransparent; //for interactables like doors, etc

    /*
    [SerializeField]
    [Tooltip("Left boundary of camera travel, relative to center of RoomVolume")]
    float LeftBound;
    [SerializeField]
    [Tooltip("Right boundary of camera travel, relative to center of RoomVolume")]
    float RightBound;
    */

    float LeftBound;
    float RightBound;

    Vector3 NewPos;

    Vector3 vel;

    bool isActive;

    
    public override void OnNotify(string msg, object obj) { }

    // Use this for initialization
    void Start () {
        mainCam = Camera.main;
        pl = GameObject.FindGameObjectWithTag("Player"); //eh.

        LeftBound = GetComponent<BoxCollider>().size.x / 2;
        RightBound = LeftBound; //*shrug*
        
        opaque = Color.grey;
        transparent = Color.clear;
        semitransparent = Color.white;
        semitransparent.a = .25f;

        NewPos = mainCam.transform.position;
    }
	
	// Update is called once per frame
	void Update () {

        if (isActive) //TODO: instead of this, give player a 'current room' variable?
        {
            NewPos.z = Mathf.Clamp(pl.transform.position.z - DistanceZ, transform.position.z - LimitZ, float.MaxValue);

            NewPos.x = Mathf.Clamp(pl.transform.position.x,
                transform.position.x - LeftBound,
                transform.position.x + RightBound);

            /*
            NewPos.x = transform.position.x - ((transform.position.x - NewPos.x) * .5f);

            if (Mathf.Clamp(mainCam.transform.position.x,
                transform.position.x - LeftBound,
                transform.position.x + RightBound) != mainCam.transform.position.x) //if camera is outside of range, smooth towards newpos
            {
                mainCam.transform.position = ();
            }
            else //otherwise, just set it
            {

            }

            Vector3.SmoothDamp(mainCam.transform.position, NewPos, ref vel, CamSmooth);
            */

            mainCam.transform.position = NewPos;


        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player") //egh
        {
            isActive = true;
            foreach (MeshRenderer Wall in Walls)
            {
                if (Wall != null) { //make sure wall exists 
                    if (Wall.GetComponent<Interactable>()) //keep interactable stuff visible
                        DOTween.To(() => Wall.material.color, x => Wall.material.color = x, semitransparent, .2f);
                    else
                        DOTween.To(() => Wall.material.color, x => Wall.material.color = x, transparent, .2f);
                }
            }
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            isActive = false;
            foreach (MeshRenderer Wall in Walls)
            {
                    DOTween.To(() => Wall.material.color, x => Wall.material.color = x, opaque, .2f);
            }
        }
    }
}
