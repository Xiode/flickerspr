﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using NotifySystem;

public class Door : Interactable {

    Collider c;
    GameObject pl;


    [SerializeField]
    [Tooltip("Is this travelled by moving towards or away from the camera?")]
    bool DepthMode = false;

    [SerializeField]
    bool Locked = false;

    [SerializeField]
    float DoorTraverseTime = .3f;

    float DoorTraverseTimeCounter = 0f;
    
    public override void OnNotify(string msg, object obj) { }

    // Use this for initialization
    void Start () {
        c = GetComponent<Collider>();

        pl = GameObject.FindGameObjectWithTag("Player");

        AddObserver(pl.GetComponent<PlayerMovement>());
        AddObserver(pl.GetComponent<PlayerUI>());
    }
	
	// Update is called once per frame
	void Update () {
		if (DoorTraverseTimeCounter > 0)
        {
            DoorTraverseTimeCounter -= Time.deltaTime;
            if (DoorTraverseTimeCounter <= 0)
            {
                Notify("Game_FinishOpenDoor");
                c.enabled = true;
            }
        }
	}

    public override void Exec()
    {
        if (!Locked)
        {
            Notify("UI_Use");
            c.enabled = false;
            if (!DepthMode)
                Notify("Game_OpenDoor", new Vector3(
                    pl.transform.position.x < transform.position.x ? 1f : -1f, //determine which side of door player's on
                    0f,     //no y movement
                    0f));   //ditto
            else
                Notify("Game_OpenDoor", new Vector3(
                    0f, //no x movement
                    0f,
                    pl.transform.position.z < transform.position.z ? 1f : -1f));

            DoorTraverseTimeCounter = DoorTraverseTime; //start timer
        } else
        {
            Notify("UI_Cancel");
        }
    }
}
