﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NotifySystem;

public class PlayerCombat : Member {
    
    private PlayerMovement playerMovement;
    private Vitality vSelf;
    
    float CanAttack = 0f;
    public WeaponStats currentWeapon;

    private float weaponTimer = 0f;
    private bool weaponPunished = false;

    public List<Cooldown> Cooldowns = new List<Cooldown>();
    private List<Cooldown> cooldownsCompleted = new List<Cooldown>();

    public AudioClip swingSound;
    public AudioClip hitSound;



    public override void OnNotify(string msg, object obj) { }

    // Use this for initialization
    void Start () {
        GameObject pl = GameObject.FindGameObjectWithTag("Player");

        vSelf = pl.GetComponent<Vitality>();
        playerMovement = pl.GetComponent<PlayerMovement>();
    }
	
	// Update is called once per frame
	void Update () {
        
        if (Input.GetKeyDown(KeyCode.Space)) // TODO: don't hardcode keys
        {
            if (CanAttack <= currentWeapon.TimingRange)
            {
                Attack();
                weaponPunished = false;
            }
            else if (CanAttack > currentWeapon.TimingRange && !weaponPunished)
            {
                CanAttack += currentWeapon.PunishTiming;
                weaponPunished = true;
                Debug.Log("Fuck yuo");
            }
        }
        if (CanAttack > 0)
            CanAttack -= Time.deltaTime;

        foreach (Cooldown c in Cooldowns)
        {
            if (c.Update()) //returns true when finished. updating here seems kinda awful.
                cooldownsCompleted.Add(c); //on complete, mark for removal. I wish cooldowns could handle this, but it's not worth fussing about at the moment
        }
        foreach (Cooldown cc in cooldownsCompleted)
        {
            Cooldowns.Remove(cc);
        }
        cooldownsCompleted.Clear(); //dump list
    }

    void Attack()
    {
        CanAttack = currentWeapon.OptimalTiming;

        playerMovement.Anim.SetTrigger("Attack");

        playerMovement.audioSource.PlayOneShot(swingSound, .5f);

        Cooldowns.Add(new Cooldown(.1f, new System.Action( () => 
            {
                Collider[] hits = Physics.OverlapBox(transform.position, new Vector3(1, 1, 1));
                Vitality v;
                foreach (Collider hit in hits)
                {
                    v = hit.gameObject.GetComponent<Vitality>();

                    if (v && v.isActiveAndEnabled && v != vSelf)
                    {
                        v.Hurt(1);
                        playerMovement.audioSource.PlayOneShot(hitSound, .8f);
                    }
                }
            } )));
    }
    
}

public class SoundEffect
{
    public string Name;
    public AudioClip Sound;

    public void Play()
    {
        // TODO: do the thing where you instantiate an audio source?
    }
}