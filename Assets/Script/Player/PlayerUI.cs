﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using NotifySystem;
using DG.Tweening;

//Handles canvas elements, UI sound effects
public class PlayerUI : Member {
    
    public Canvas UICanvas;
    CanvasScaler UICanvasScaler;
    public EventSystem UIEventSystem;

    [SerializeField]
    RectTransform TopLetterboxer;
    [SerializeField]
    RectTransform BottomLetterboxer;
    public float LetterboxScale;

    private GameObject pl;

    public bool InDialogue; //is player in dialogue?
    private bool CompleteDialogue;

    //this is definitely a crap way to do this, but whatever
    [SerializeField]
    AudioSource UIAudioA; //handles ui one-shots
    [SerializeField]
    AudioSource UIAudioB; //handles looping audio (music)

    [SerializeField]
    AudioClip UISoundDialogueTick;
    [SerializeField]
    AudioClip UISoundDialogueEnter;
    [SerializeField]
    AudioClip UISoundDialogueExit;
    [SerializeField]
    AudioClip UISoundUse;
    [SerializeField]
    AudioClip UISoundCancel;
    [SerializeField]
    AudioClip MusicTest;
    
    public override void OnNotify(string msg, object obj)
    {
        switch (msg)
        {
            case "UI_Use":
                UIAudioA.PlayOneShot(UISoundUse);
                break;
            case "UI_Cancel":
                UIAudioA.PlayOneShot(UISoundCancel);
                break;
            case "UI_DialogueTick":
                UIAudioA.PlayOneShot(UISoundDialogueTick);
                break;
            case "UI_DialogueEnter":
                UIAudioA.PlayOneShot(UISoundDialogueEnter);
                EnterDialogue();
                break;
            case "UI_DialogueExit":
                UIAudioA.PlayOneShot(UISoundDialogueExit);
                ExitDialogue();
                break;
        }
    }

    // Use this for initialization
    void Start () {
        pl = GameObject.FindGameObjectWithTag("Player");
        AddObserver(pl.GetComponent<PlayerMovement>());

        UICanvasScaler = UICanvas.GetComponent<CanvasScaler>();

        UIAudioB.clip = MusicTest;
        UIAudioB.Play();
	}
	
	// Update is called once per frame
	void Update ()
    {
    }

    public void UIButtonInput(int selection) //this is called when a UI button is pressed
    {

    }

    public void EnterDialogue()
    {
        InDialogue = true;
        Notify("Input_FreezeMotion");
        RaiseLetterbox();
    }

    public void ExitDialogue()
    {
        //pl.GetComponent<PlayerMovement>().Pause = false;
        //PopAllDialogues();

        DropLetterbox();
        Notify("Input_UnfreezeMotion");
        Notify("Input_DelayUse"); //ask to disable the use key for a moment, to prevent quick mashing leading to re-entering dialogue (REAL FRUSTRATING)
    }

    private void RaiseLetterbox()
    {
        //set the proper scale according to window size.
        TopLetterboxer.sizeDelta = new Vector2(UICanvasScaler.referenceResolution.x, UICanvasScaler.referenceResolution.y * LetterboxScale);
        BottomLetterboxer.sizeDelta = new Vector2(UICanvasScaler.referenceResolution.x, UICanvasScaler.referenceResolution.y * LetterboxScale);

        TopLetterboxer.DOScaleY(1f, .2f);
        BottomLetterboxer.DOScaleY(1f, .2f);
    }

    private void DropLetterbox()
    {
        TopLetterboxer.DOScaleY(0f, .2f);
        BottomLetterboxer.DOScaleY(0f, .2f);
    }
}
