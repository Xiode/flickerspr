﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NotifySystem;


//this goes on the trigger empty.
public class PlayerInteraction : Member {

    [SerializeField]
    GameObject BangPrefab;

    public float CannotUse = 0f;
    
    public override void OnNotify(string msg, object obj) { }
    
    PlayerUI plui;

    // Use this for initialization
    void Start () {
        plui = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerUI>(); //gross.
        AddObserver(plui);
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetButtonDown("Exit"))
        {
            Application.Quit();
        }

        if (Input.GetButtonDown("Use") && CannotUse <= 0f)
        {
            Collider[] hits = Physics.OverlapBox(transform.position, new Vector3(1,1,1));
            Interactable i;
            foreach (Collider hit in hits)
            {
                i = hit.gameObject.GetComponent<Interactable>();

                if (i && i.isActiveAndEnabled)
                    i.Exec();
            }
        } else if (CannotUse > 0f)
        {
            CannotUse -= Time.deltaTime;
        }
    }

    Dictionary<GameObject, GameObject> bangs = new Dictionary<GameObject, GameObject>();
    
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<Interactable>())
        {
            GameObject b = Instantiate(BangPrefab);
            Vector3 pos = other.gameObject.transform.position;
            pos.y += 2f;
            b.transform.position = pos;
            bangs.Add(other.gameObject, b);
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.GetComponent<Interactable>())
        {
            if (bangs[other.gameObject])
            {
                GameObject.Destroy(bangs[other.gameObject]); //destroy bang
                bangs.Remove(other.gameObject); //remove from dictionary
            }
        }
    }
}
