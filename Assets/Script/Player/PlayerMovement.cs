﻿/*
PlayerMovement.cs

Handles Player Movement, Input, and Animations
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using NotifySystem;
using DG.Tweening;

public class PlayerMovement : Member {

    public GameRules gameRules;

    [SerializeField]
    float Speed = 1.0f;

    [SerializeField]
    float GravityModifier = 10.0f;

    [SerializeField]
    float JumpPower = 5f;

    private CharacterController cc;

    [HideInInspector]
    public AudioSource audioSource;

    [SerializeField]
    private SpriteRenderer spriteRenderer;

    [SerializeField]
    [Tooltip("This object's X Scale will be set to -1/1 according to the spriteRenderer's facing")]
    private Transform Attached;

    public Stats stats;
    
    public Animator Anim;

    public bool FreezeMotion = false;

    private Vector3 movement = Vector3.zero;
    private Vector3 forcemovement = Vector3.zero;
    private bool forcemovementactive = false;

    public float JumpCheckDistance;
    public float JumpCheckArea;
    public LayerMask JumpCheckLayerMask;
    
    public override void OnNotify(string msg, object obj)
    {
        switch (msg)
        {
            case "Game_OpenDoor":
                forcemovement = (Vector3)obj;
                forcemovementactive = true;
                break;
            case "Game_FinishOpenDoor":
                forcemovement = Vector3.zero;
                forcemovementactive = false;
                break;
            case "Input_FreezeMotion":
                FreezeMotion = true;
                //print("Frozen!");
                break;
            case "Input_UnfreezeMotion":
                FreezeMotion = false;
                //print("Unfrozen!");
                break;
        }
    }

    void Start ()
    {
        //GroundCheckLayerMask

        DOTween.Init(); //init DOTween. unnecessary?

        Application.targetFrameRate = 300;
        QualitySettings.vSyncCount = 0; //should probably do these elsewhere :x

        cc = GetComponent<CharacterController>();
        audioSource = GetComponent<AudioSource>();

    }

    void Update()
    {
        if (forcemovementactive) //gross.
        {
            movement.x = forcemovement.x * Speed;
            movement.z = forcemovement.z * Speed;
        }
        else if (!FreezeMotion) //still gross.
        {
            movement.x = Input.GetAxisRaw("Horizontal") * Speed;
            movement.z = Input.GetAxisRaw("Vertical") * Speed;

            //boxcast to check if we're on the ground. more consistent than isGrounded
            //if (Physics.Linecast(transform.position, transform.position+(Vector3.down*.1f), GroundCheckLayerMask))
            if (Physics.BoxCast(transform.position, new Vector3(JumpCheckArea, .01f,JumpCheckArea),Vector3.down,transform.rotation, JumpCheckDistance, JumpCheckLayerMask))
            {
                if (cc.isGrounded)
                    movement.y = 0f;
                if (Input.GetButtonDown("Jump")) //jump! - make a better input solution
                {
                    movement.y = JumpPower;
                }
            }
        }
        else
        {
            movement.x = 0;
            movement.z = 0;
        }



        movement.y -= Time.deltaTime * gameRules.Gravity * GravityModifier;

        if (movement.x > 0.01 && spriteRenderer.flipX)
        {
            spriteRenderer.flipX = false;
            Attached.localScale = new Vector3(-1,1,1);
        }
        else if (movement.x < -0.1f && !spriteRenderer.flipX)
        {
            spriteRenderer.flipX = true;
            Attached.localScale = new Vector3(1, 1, 1);
        }

        if (movement.z > 0.01)
        {
            //spriteMeshRenderer.material = SpriteBack;
        }
        else if (movement.z < -0.1f)
        {
            //spriteMeshRenderer.material = SpriteFront;
        }

        cc.Move(movement * Time.deltaTime);
	}
}