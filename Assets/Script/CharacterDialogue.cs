﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using NotifySystem;


public class CharacterDialogue : Interactable
{
    public string CharacterName;

    public DialogueNode EntryNode;

    [SerializeField]
    GameObject SpeechBubblePrefab;

    SpeechBubble activeSpeechBubble;
    DialogueLineNode activeLineNode;
    
    PlayerUI plui;

    bool InDialogue = false;

    void Start()
    {
        Notify("CharacterTransform_" + CharacterName, this.gameObject.transform);

        plui = (GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerUI>());
        AddObserver(plui);

        Notify("CharacterTransform_" + CharacterName, this.gameObject.transform); // TODO: might cause performance issues?
    }

    public override void OnNotify(string msg, object obj) { }

    public override void Exec()
    {
        Notify("CharacterTransform_" + CharacterName, this.gameObject.transform); // TODO: might cause performance issues?
        Notify("CharacterTransform_Player", plui.gameObject.transform);


        if (InDialogue)
        {
            if (activeSpeechBubble && !activeSpeechBubble.isFinished)
            {
                activeSpeechBubble.Complete();
            }
            else
            {

                if (activeLineNode == null || activeLineNode.NextNode() == null || activeLineNode.NextNode().Line() == null)
                {
                    PopDialogue(activeSpeechBubble); //pop old dialogue

                    InDialogue = false;
                    Notify("UI_DialogueExit");
                    Notify("UI_Use");
                }
                else
                {

                    activeLineNode = activeLineNode.NextNode().Line();
                    Notify("UI_Use");
                    SayLine();
                }

            }
        }
        else
        {
            InDialogue = true;

            Notify("UI_DialogueEnter");
            Notify("UI_Use");

            activeLineNode = EntryNode.Line();
            SayLine();
        }

    }

    //try to say a line
    void SayLine()
    {
        PopDialogue(activeSpeechBubble); //pop old dialogue
        
        Vector3 position = ((Transform)FactDictionary.GetFact("CharacterTransform_" + activeLineNode.CharacterID).Value).position;

        position.y += 2.5f;

        activeSpeechBubble = PushDialogue(activeLineNode.Line().Text, position); //push new dialogue

        Notify("CharacterDialogueLast_" + CharacterName, activeLineNode.name);
        Notify(activeLineNode.name);
    }


    SpeechBubble PushDialogue(string Line, Vector3 position)
    {
        Vector2 newPos = Camera.main.WorldToViewportPoint(position);
        newPos.x *= plui.UICanvas.pixelRect.width;
        newPos.y *= plui.UICanvas.pixelRect.height;

        GameObject bubble = Instantiate(SpeechBubblePrefab, plui.UICanvas.transform);
        SpeechBubble b = bubble.GetComponent<SpeechBubble>();
        
        bubble.transform.position = newPos;

        b.Init(Line);

        b.AddObserver(plui);

        return b;
    }

    void PopDialogue(SpeechBubble s)
    {
        if (!s)
            return; //doesn't exist

        plui.RemoveObserver(s);
        GameObject.Destroy(s.gameObject);
    }

}



/*
//This component goes on a character that you want the player to be able to talk to
public class Dialogue : Interactable {
    
    [SerializeField]
    GameObject SpeechBubblePrefab;

    [SerializeField] [Tooltip ("List characters involved in dialogue, including player. First character = 0, second = 1, etc.")]
    DialogueCharacter[] Characters;

    [SerializeField]
    DialogueLine[] Lines;

    [SerializeField]
    DialogueLine[] AlreadyTalkedToLines;

    PlayerUI plui;

    SpeechBubble activeDialogue;
   

    int ticker = 0;
    bool talked = false; //has the player talked to this npc once already? (todo: should there be multiple states?)
    bool readyToExit = false; //is player ready to exit?

    
    public override void OnNotify(string msg, object obj) { }

    void Start()
    {
        plui = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerUI>(); //gross.
        AddObserver(plui);
    }

    public override void Exec()
    {
        //ONLY 1 of 4 things should occur:
        //1. enter dialogue
        //2. continue dialogue
        //3. force complete line
        //4. exit dialogue
        
        //1. enter dialogue
        if (ticker == 0)
        {
            //plui.EnterDialogue(); 
            readyToExit = false;
            Notify("UI_DialogueEnter");

            //FOR DEBUG PURPOSES - this shows who's involved in dialogue
            string _ = "Entered dialogue with ";
            for (int i = 0; i < Characters.Length; i++)
            {
                _ += i == 0 ? Characters[i].Name : (", " + Characters[i].Name);
            }
            //print(_);
            //end debug

        }


        //4. exit dialogue
        if (readyToExit)
        {
            if (activeDialogue.isFinished) //if the last dialogue box has been fully shown to the player, exit dialogue
            {
                ConcludeDialogue();
            }
            else //if the last dialogue box is still in progress, complete it.
            {
                activeDialogue.Complete();
            }
        }
        else
        {
            if (activeDialogue && !activeDialogue.isFinished) {
                activeDialogue.Complete();
                return;
            }

            if (!talked)
            { //say first time lines
                SayLine(
                    GetDialogueLine(Lines, ticker),
                    Characters[Lines[ticker].CharID].gameObject.transform.position);
                Notify("UI_Use");
            }
            else { //say repeat lines
                SayLine(
                    GetDialogueLine(AlreadyTalkedToLines, ticker),
                    Characters[AlreadyTalkedToLines[ticker].CharID].gameObject.transform.position);
                Notify("UI_Use");
            }
            
            if (!talked && ticker >= Lines.Length)
            {
                readyToExit = true;
            }
            if (talked && ticker >= AlreadyTalkedToLines.Length)
            {
                readyToExit = true;
            }
        }
    }

    string GetDialogueLine(DialogueLine[] lines, int tick)
    {
        if (tick >= lines.Length)
        {
            print("shouldn't be reached! " + tick);
            return "";
        }
        return (Characters[lines[tick].CharID].Name + ": " + lines[tick].Line);
    }

    void ConcludeDialogue()
    {
        talked = true;
        ticker = 0;
        PopDialogue(activeDialogue);

        Notify("UI_DialogueExit");
        //plui.CloseDialogue(); //exit dialogue mode (unfreeze player, drop letterbox, etc)
        activeDialogue = null;
    }

    void SayLine(string Line, Vector3 position)
    {
        //plui.PopAllDialogues();//pop all dialogue. not ideal - what if there's background dialogue? (think 'character mumbling to themselves')
        //lol fixed??
        if (activeDialogue && !activeDialogue.isFinished) //if we're still in the middle of dialogue, force complete it. don't increment
        {
            activeDialogue.Complete();
        }
        else
        {
            if (Line == "")
                return;

            position.y += 2.5f;

            PopDialogue(activeDialogue); //pop old dialogue
            activeDialogue = PushDialogue(Line, position); //push new dialogue
            //activeDialogue = plui.PushDialogue(Line, position);
            ticker++;
        }

    }

    
    SpeechBubble PushDialogue(string Line, Vector3 position)
    {
        Vector2 newPos = Camera.main.WorldToViewportPoint(position);
        newPos.x *= plui.UICanvas.pixelRect.width;
        newPos.y *= plui.UICanvas.pixelRect.height;

        GameObject bubble = Instantiate(SpeechBubblePrefab);
        SpeechBubble b = bubble.GetComponent<SpeechBubble>();
        //dialogues.Add(bubble);
        bubble.transform.SetParent(plui.UICanvas.transform);
        bubble.transform.position = newPos;
        //bubble.GetComponentInChildren<UnityEngine.UI.Text>().text = Text;
        b.Init(Line);

        b.AddObserver(plui);

        return b;
    }

    void PopDialogue(SpeechBubble s)
    {
        if (!s)
            return; //doesn't exist

        plui.RemoveObserver(s);
        GameObject.Destroy(s.gameObject);
    }
}

class DialogueLine : ScriptableObject
{
    public int CharID;
    [TextArea(2,5)]
    public string Line;

    public DialogueChoice[] Choices;
}

[System.Serializable]
class DialogueCharacter : System.Object
{
    public string Name;
    public GameObject gameObject;
}

[System.Serializable]
class DialogueChoice
{
    DialogueLine DefaultChoice;

}*/
