﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using DG.Tweening;

public class DoorwayRevealer : MonoBehaviour {

    [SerializeField]
    bool InOutMode = false;

    [SerializeField] [Tooltip("Right objects in normal mode / the objects you walk through in InOut mode")]
    MeshRenderer[] RightWalls;
    [SerializeField] [Tooltip("Left objects in normal mode / the objects near the camera in InOut mode")]
    MeshRenderer[] LeftWalls;

    private Color opaque;
    private Color transparent;

    // Use this for initialization
    void Start () {
        opaque = Color.grey;
        transparent = Color.clear;
        transparent.a = .1f;
	}

    void OnTriggerEnter(Collider other)
    {
        //hide both walls until player exits
        foreach (MeshRenderer r in RightWalls)
        {
            DOTween.To(() => r.material.color, x => r.material.color = x, transparent, .2f);
        }

        foreach (MeshRenderer l in LeftWalls)
        {
            DOTween.To(() => l.material.color, x => l.material.color = x, transparent, .2f);
        }

            //LeftWall.material.color = transparent;//ditto
        }

    void OnTriggerExit(Collider other)
    {
        if (InOutMode)
        {
            if (other.transform.position.z > transform.position.z) //if other object (player) is behind me on exit, hide the right wall and return the left wall
            {
                foreach (MeshRenderer l in LeftWalls)
                {
                    //DOTween.To(() => l.material.color, x => l.material.color = x, opaque, .3f);
                }
            }
            else
            {
                foreach (MeshRenderer r in RightWalls)
                {
                    DOTween.To(() => r.material.color, x => r.material.color = x, opaque, .3f);
                }
            }
        }
        else //left-right mode (default)
        {
            if (other.transform.position.x > transform.position.x) //if other object (player) is right of me on exit, hide the right wall
            {
                foreach (MeshRenderer l in LeftWalls)
                {
                    DOTween.To(() => l.material.color, x => l.material.color = x, opaque, .3f);
                }

                //RightWall.material.color = transparent; //TODO: Tweening, don't force color (100.0f,100.0f,100.0f)
                //LeftWall.material.color = opaque;//ditto
            }
            else //do the opposite
            {
                foreach (MeshRenderer r in RightWalls)
                {
                    DOTween.To(() => r.material.color, x => r.material.color = x, opaque, .3f);
                }
            }
        }

    }
}
