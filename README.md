# README #

### Flicker SP ###

This project is a Unity game based loosely on Kazerad's Flicker prototype.

### How do I open the project? ###

* Download the latest Unity editor at https://unity3d.com
* Clone this repo
* Install DOTween to Scripts/Util/DOTween, as the repo doesn't carry binaries: http://dotween.demigiant.com
* Open the folder as a project from Unity's project manager interface

### How do I play the latest binary build? ###

Visit the 'downloads' section, and download "FlickerSP-2017-MM-DD.7z" for stable builds.